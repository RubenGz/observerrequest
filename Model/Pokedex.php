<?php


namespace Gamma\PokeAPI\Model;

use Gamma\PokeAPI\Api\ConnectionInterface;
use Gamma\PokeAPI\Api\Data\PokemonInterface;
use Gamma\PokeAPI\Api\Data\PokemonInterfaceFactory;
use Gamma\PokeAPI\Api\PokedexInterface;
use Magento\Framework\App\CacheInterface;
use Magento\Framework\Serialize\Serializer\Base64Json;

class Pokedex implements PokedexInterface
{
    /**
     * @var Connection
     */
    protected $connection;

    /**
     * @var PokemonInterfaceFactory
     */
    protected $pokemonFactory;

    protected $cache;

    protected $base64;

    public function __construct(
        ConnectionInterface $connection,
        PokemonInterfaceFactory $pokemonFactory,
        CacheInterface $cache,
        Base64Json $base64
    )
    {
        $this->connection = $connection;
        $this->pokemonFactory = $pokemonFactory;
    }

    public function getPokemon(string $name): PokemonInterface
    {
        $pokemonData = $this->connection->get("pokemon/{$name}");

        $pokemon = $this->pokemonFactory->create();

        $pokeTypes = $this->processTypes($pokemonData['types']);

        $moves = $this->processMoves($pokemonData['moves']);

        $region = $this->getRegion($name);

        $flavorText = $this->getFlavorText($name);

        $pokemon->setName(ucfirst($pokemonData['name']))
            ->setType(implode(' ', $pokeTypes))
            ->setImage($pokemonData['sprites']['front_default'])
            ->setMoves($moves)
            ->setRegion($region)
            ->setFlavorText($flavorText);

        return $pokemon;
    }

    protected function processTypes(array $pokemonTypeData)
    {
        return array_map(function ($type) {
            return ucfirst($type['type']['name']);
        }, $pokemonTypeData);
    }

    protected function processMoves(array $pokemonMoveData)
    {
        return array_map(function ($move) {
            return ucfirst(str_replace('-', ' ', $move['move']['name']));
        }, $pokemonMoveData);
    }

    protected function getRegion(string $pokemonName): string
    {
        $detailedData = $this->getSpeciesData($pokemonName);

        $generation = $detailedData['generation']['url'];

        $generationId = explode('generation/', $generation)[1];

        $generationData = $this->connection->get("generation/{$generationId}");

        return ucfirst($generationData['main_region']['name']);
    }

    protected function getSpeciesData(string $pokemonName): array
    {
        return $this->connection->get("pokemon-species/{$pokemonName}");
    }

    protected function getFlavorText(string $pokemonName)
    {
        $detailedData = $this->getSpeciesData($pokemonName);

        foreach ($detailedData['flavor_text_entries'] as $textEntry) {
            if ($textEntry['language']['name'] === 'en') {
                return $textEntry['flavor_text'];
            }
        }

        return 'We do not know anything about this guy!';
    }
}