<?php


namespace Gamma\PokeAPI\Observer;


use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Psr\Log\LoggerInterface;

class Pokedex implements ObserverInterface
{
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $request = $observer->getEvent()->getData('request');

        if(!$request->getParam('pokemon')){
            return;
        }

        $request->setParam('pokemon','meganium');

        $this->logger->info(
            __('Pokemon requested %1', $request->getParam('pokemon'))
        );

    }
}